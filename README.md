# Quiz App with Python and Flask

Code following the tutorial 
https://radiusofcircle.blogspot.com/2016/03/making-quiz-website-with-python.html

Tested to be working with Python 3.7

## How to use it

Setup:

```shell
$ git clone git clone https://bitbucket.org/staeff/flaskquiz.git
$ cd flaskquiz
$ python3 -m venv .
$ source ./bin/activate
$ pip install -r requirements.txt
```

Run the App:

```shell
$ python quiz.py
```

After running the last command the server runs on http://127.0.0.1:5000/. Open that page in your browser and have fun :)